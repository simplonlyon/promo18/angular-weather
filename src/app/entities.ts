export interface City {
    name:string;
    insee:string;
    latitude:number;
    longitude:number;
}