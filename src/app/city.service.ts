import { Injectable } from '@angular/core';
import { City } from './entities';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  list:City[] = [
    {
      name:"Paris",
      insee:"75056",
      latitude:48.50,
      longitude:2.20
    },
    {
      name:"Marseille",
      insee:"13055",
      latitude:43.18,
      longitude:5.23
    },
    {
      name:"Lyon",
      insee:"69123",
      latitude:45.45,
      longitude:4.50
    }
  ];

  constructor() { }

  getAll() {
    return this.list;
  }

  getById(id:number) {
    return this.list[id];
  }
}
