import { TestBed } from '@angular/core/testing';

import { WeatherAPIClientService } from './weather-apiclient.service';

describe('WeatherAPIClientService', () => {
  let service: WeatherAPIClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WeatherAPIClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
