import { Component, OnInit } from '@angular/core';
import { CityService } from '../city.service';
import { City } from '../entities';
import { WeatherAPIClientService } from '../weather-apiclient.service';

@Component({
  selector: 'app-weather-page',
  templateUrl: './weather-page.component.html',
  styleUrls: ['./weather-page.component.css']
})
export class WeatherPageComponent implements OnInit {

  // Attributs pour l'afichage des informations
  dataWeatherAPI = {
    "today" : "",
    "tomorrow" : "",
    "dayaftertomorrow": ""
  }
  dataMeteoConcept:any = {
    "today" : "",
    "tomorrow" : "",
    "dayaftertomorrow": ""
  };
  // Attributs pour le formulaire
  displayFormModal = false;
  listCities?:City[];
  selectedCity:number = 0;

  constructor(private ws:WeatherAPIClientService, private cs:CityService) { }

  ngOnInit(): void {
    this.listCities = this.cs.getAll();
    this.getAllData();
  }

  toggleFormModal() {
    this.displayFormModal = !this.displayFormModal;
  }

  handleCityChange() {
    this.displayFormModal = false;
    this.getAllData();
  }

  getAllData() {
    let currentCity:City = this.listCities![this.selectedCity];
    this.getDataFromWeatherAPI(currentCity?.name);
    this.getDataFromOpenWeather();
    this.getDataFromMeteoConcept(currentCity?.insee);
  }

  getDataFromOpenWeather() {
  }

  getDataFromMeteoConcept(insee:string) {
    this.ws.getMeteoConceptForecast(insee).subscribe(data => {
      this.dataMeteoConcept.today = this.formatStringTemperatureFromMeteoConcept(data, 0);
      this.dataMeteoConcept.tomorrow = this.formatStringTemperatureFromMeteoConcept(data, 1);
      this.dataMeteoConcept.dayaftertomorrow = this.formatStringTemperatureFromMeteoConcept(data, 2);
    })
  }

  getDataFromWeatherAPI(city:string) {
    this.ws.getWeatherAPIForecast(city).subscribe(
      data => {
        let temp_data:any = data;
        // Températures pour aujourd'hui
        this.dataWeatherAPI.today = this.formatStringTemperatureFromWeatherAPI(temp_data, 0);
        // Températures pour demain
        this.dataWeatherAPI.tomorrow = this.formatStringTemperatureFromWeatherAPI(temp_data, 1);
        // Températures pour après-demain
        this.dataWeatherAPI.dayaftertomorrow = this.formatStringTemperatureFromWeatherAPI(temp_data, 2);
      }
    );
  }

  formatStringTemperatureFromWeatherAPI(weatherdata:any, _day:number) {
    return weatherdata.forecast!.forecastday[_day]!.day.mintemp_c + "°C - " + weatherdata.forecast!.forecastday[_day]!.day.maxtemp_c + "°C";
  }

  formatStringTemperatureFromMeteoConcept(data:any, _day:number) {
    return data.forecast[_day].tmin + "°C - " + data.forecast[_day].tmax + "°C";
  }

}
