import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherAPIClientService {

  constructor(private http:HttpClient) { }

  getWeatherAPIForecast(city:string) {
    return this.http.get("https://api.weatherapi.com/v1/forecast.json?key=" + environment.weatherAPItoken + "&q=" + city + "&days=3&aqi=no&alerts=no");
  }

  getOpenWeatherForecast(/* .... */ ) {
    // Appel à l'API OpenWeather
  }

  getMeteoConceptForecast(insee:string) {
    return this.http.get('https://api.meteo-concept.com/api/forecast/daily?token=' + environment.meteoConceptToken + '&insee=' + insee);
  }
}
